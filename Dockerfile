FROM openjdk:17-alpine
VOLUME /tmp
ADD /build/libs/*.jar spring-petclinic-3.0.0.jar
EXPOSE 8080
ENTRYPOINT exec java -jar spring-petclinic-3.0.0.jar
